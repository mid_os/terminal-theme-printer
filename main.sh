#!/bin/sh


BACKGROUNDS="40m 41m 42m 43m 44m 45m 46m 47m"
TEXT_COLORS="m 1m 30m 1;30m 31m 1;31m 32m 1;32m 33m 1;33m 34m 1;34m 35m 1;35m 36m 1;36m 37m 1;37m"

for header in def $BACKGROUNDS; do
  printf "\t $header"
done
echo

row() {
  TEXT_COLOR=$1

  printf "${TEXT_COLOR}\t\e[${TEXT_COLOR} text\e[0m"
  for BACKGROUND in $BACKGROUNDS; do
    printf "\t\e[${BACKGROUND}\e[${TEXT_COLOR} text \e[0m"
  done
  printf "\n"
}


for COLOR in $TEXT_COLORS; do
  row $COLOR
done
