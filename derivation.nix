{ stdenv, fetchFromGitLab }:

let 
  name = "terminal-theme-printer";
  version = "v0.0.1";
in stdenv.mkDerivation {
  inherit name;
  inherit version;

  src = fetchFromGitLab {
    owner = "mid_os";
    repo = name;
    rev = version;
    sha256 = "sha256-wzeAvE58b7Idj7ur0ssExokcn17p5sH4x9Gc9R10Z54=";
  };

  installPhase = ''
    mkdir -p $out/bin
    cp main.sh $out/bin/terminal-theme-printer
  '';
}
